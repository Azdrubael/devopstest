export const commands = {
    help: 'help',
    getItems: 'getItems',
    setTitle: 'setTitle',
    exit: 'exit',
}

export const messages = {
    enterCommand: 'Please enter the command\n',
    unsupportedCommand: 'Unsupported command!\n',
    enterTaskId: 'Enter task\' id: ',
    enterTitle: 'Enter task\' new title: ',
    bye: 'Good bye!',
}

export const prompt: string =
    'COMMANDS:\n'+
    `  ${commands.help}         show list of commands\n`+
    `  ${commands.getItems}     get and print items from your azure devops\n`+
    `  ${commands.setTitle}     change item title\n`+
    `  ${commands.exit}         exit from the program`;

export const query = (project: string) => {
    return `select [System.Id], [System.Title],[System.AssignedTo],
    [System.WorkItemType],[Microsoft.VSTS.Scheduling.StoryPoints],[Microsoft.VSTS.Common.Priority],
    [Microsoft.VSTS.Scheduling.Effort],
    [System.State],
    [System.IterationPath]
    FROM WorkItemLinks
    WHERE
    ([Source].[System.TeamProject]='${project}'
    and [Source].[System.WorkitemType]<>''
    )

    and ([System.Links.LinkType]='System.LinkTypes.Hierarchy-Forward')
    and ([Target].[System.WorkItemType] <> '' )
    ORDER BY [System.Id]
   mode (Recursive)`
}
