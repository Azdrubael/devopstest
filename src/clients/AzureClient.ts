import * as azdev from "azure-devops-node-api";
import * as Constants from '../Constants';
import { Configuration } from '../Configuration';
import { Wiql } from "azure-devops-node-api/interfaces/WorkItemTrackingInterfaces";
import { IWorkItem } from '../IWorkItem';
import * as vss from 'azure-devops-node-api/interfaces/common/VSSInterfaces';

export class AzureClient {
    private connection: azdev.WebApi;
    constructor() {
        let authHandler = azdev.getPersonalAccessTokenHandler(Configuration.personalAccessToken);

        this.connection = new azdev.WebApi(Configuration.azureOrgUrl, authHandler);
    }

    public async getWorkItems(): Promise<IWorkItem[]> {
        let workItemTrackingApi = await this.connection.getWorkItemTrackingApi();

        const wiql: Wiql = {
            query: Constants.query(Configuration.projectName)
        }

        let items = await workItemTrackingApi.queryByWiql(wiql);

        if (items.workItemRelations) {
            return items.workItemRelations?.map(item => {
                return {
                    id: item.target?.id,
                    parentId: item.source?.id,
                } as IWorkItem;
            });
        }
        return [];
    }

    public async getWorkItemsInfo(items: IWorkItem[]): Promise<IWorkItem[]> {
        let workItemTrackingApi = await this.connection.getWorkItemTrackingApi();

        let ids = items.map(item => item.id);

        const workItems = await workItemTrackingApi.getWorkItems(ids);



        for (let i = 0; i < items.length; i++) {
            const workItem = workItems.find(wi => wi.id === items[i].id)

            items[i].title = workItem?.fields!['System.Title'];
            items[i].type = workItem?.fields!['System.WorkItemType'];
        }

        return items;
    }

    public async updateWorkItemTitle(id: number, newTitle: string) {
        let workItemTrackingApi = await this.connection.getWorkItemTrackingApi();

        let changes: vss.JsonPatchDocument = [{ "op": "add", "path": "/fields/System.Title", "value": newTitle }];
        await workItemTrackingApi.updateWorkItem(null, changes, id);
    }
}
