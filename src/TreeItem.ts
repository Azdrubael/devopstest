import { IWorkItem } from './IWorkItem';

export class TreeItem {
    public id: number | undefined;
    public title: string | undefined;
    public type: string | undefined;
    public children: TreeItem[];

    constructor(currentItem: IWorkItem, items: IWorkItem[]) {
        this.id = currentItem.id;
        this.title = currentItem.title;
        this.type = currentItem.type;
        this.children = [];

        let childItems = items.filter(item => item.parentId === currentItem.id);

        childItems.forEach(child => {
            const treeNode = new TreeItem(child, items);
            this.children.push(treeNode);
        });
    }

    public print(itemLevel: number = 0) {
        console.log(`|${'--'.repeat(itemLevel)} (${this.id})[${this.type}] ${this.title}`);
        this.children.forEach(child => {
            child.print(itemLevel + 1);
        });
    }
}
