import { AzureClient } from './clients/AzureClient';
import { TreeItem } from './TreeItem';
import * as readline from 'readline';
import { Configuration } from './Configuration';
import * as Constants from './Constants';

Configuration.initialize();
const azureClient = new AzureClient();

console.log(Constants.prompt);

enterCommand();

async function enterCommand() {
    let rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question(Constants.messages.enterCommand, async (answer: string) => {
        switch (answer.trim().toLowerCase()) {
            case Constants.commands.help.toLowerCase():
                rl.close();
                console.log(Constants.prompt);
                return enterCommand();
            case Constants.commands.exit.toLowerCase():
                rl.close();
                console.log(Constants.messages.bye);
                return;
            case Constants.commands.getItems.toLowerCase():
                rl.close();
                return printWorkItemTree();
            case Constants.commands.setTitle.toLowerCase():
                rl.close();
                return changeTitle();
            default:
                rl.close();
                console.log(Constants.messages.unsupportedCommand);
                return enterCommand();
        }
    });
}

async function printWorkItemTree() {
    let items = await azureClient.getWorkItems();

    items = await azureClient.getWorkItemsInfo(items);
    const parentItems = items.filter(item => !item.parentId);

    const trees = parentItems.map(parentItem => {
        return new TreeItem(parentItem, items);
    });

    trees.forEach(tree => tree.print());

    return enterCommand();
}

async function changeTitle() {
    let rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question(Constants.messages.enterTaskId, async (id) => {
        rl.question(Constants.messages.enterTitle, async (title) => {
            rl.close();
            await azureClient.updateWorkItemTitle(parseInt(id), title);
            return enterCommand();
        });
    });
}