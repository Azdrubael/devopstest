import * as dotenv from 'dotenv';

export class Configuration {
    public static personalAccessToken: string;
    public static azureOrgUrl: string;
    public static projectName: string;

    public static initialize() {
        dotenv.config();
        this.personalAccessToken = process.env.PERSONAL_ACCESS_TOKEN!;
        this.azureOrgUrl = process.env.AZURE_ORG_URL!;
        this.projectName = process.env.PROJECT_NAME!;
    }
}
