export interface IWorkItem {
    id: number;
    parentId?: number;
    title?: string;
    type?: string;
}
