Initial set up:
    Call command 'npm i' from project root

Set variables:
    Set your project's variables in .env file or use existing one for test environment

Start project
    Run command 'npm start' from project root.